﻿namespace App.Core
{
    public class Logger
    {
        private static NLog.Logger log;

        private Logger() { }

        public static NLog.Logger GetLog()
        {
            if (log == null)
                log = NLog.LogManager.GetCurrentClassLogger();
            return log;
        }
    }
}

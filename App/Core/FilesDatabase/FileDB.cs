﻿using System;
using System.Collections.Generic;
using System.IO;
using Telegram.Bot.Types;
using File = System.IO.File;

namespace App.Core.FilesDatabase
{
    public class FileDB
    {
        public static void CreateFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                Logger.GetLog().Info($"File with name {fileName} already exists");
                return;
            }

            FileStream file = new FileStream(fileName, FileMode.Create);
            file.Close();
        }

        public static bool WriteFile(string fileName, string str)
        {
            if (!FileExist(fileName))
                return false;

            File.WriteAllText(fileName, str);
            return true;
        }


        public static string ReadFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                StreamReader reader = new StreamReader(fileName);
                string text = reader.ReadToEnd();
                reader.Close();
                return text;
            }
            else
            {
                throw new InvalidOperationException("Can't read, file " + fileName + " doesnt exist");
            }
        }

        public static bool FileExist(string fileName)
        {
            return File.Exists(fileName);
        }
    }
}

﻿namespace App.Core.FilesDatabase
{
    static class Tables
    {
        public const string TABLE_POLL_NAME = "Polls.txt";
        public const string TABLE_ACCOUNT_NAME = "Account.txt";
        public const string TABLE_ID = "ID.txt";
        public const string TABLE_ACTIVE_ACCOUNTS = "ActiveAccounts.txt";
        public const string TABLE_BINANCE_DATA = "BinanceData.txt";
        public const string TABLE_PLAYERS = "Players.txt";
        public const string TABLE_ROOMS = "Rooms.txt";
        public const string TABLE_PLAYERS_ITEMS = "PlayersItems.txt";
        public const string TABLE_PLAERS_STATISTICS = "PlayersStatistics.txt";
    }
}

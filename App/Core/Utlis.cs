﻿using App.Server.Game;
using App.Server.JsonReply;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using Telegram.Bot.Types;
using Poll = App.Server.Poll;

namespace App.Core
{
    public class Utlis
    {
        public const string URI_WEBSOCKET_DEMO = "wss://demo.piesocket.com/v3/channel_123?api_key=VCXCEuvhGcBDP7XhiJJUDvR1e1D3eiVjgZ9VRiaV&notify_self";
        public const string URI_BINANCE_DEMO = "wss://stream.binance.com:9443/ws/steembtc@depth5@100ms";
        public const string TEST_SUB = "{\r\n\"method\": \"SUBSCRIBE\",\r\n\"params\":\r\n[\r\n\"btcusdt@depth\"\r\n],\r\n\"id\": 1\r\n}";
        public const string SMILENAME = "??";
        public const string SETTINGS_JSON = @"C:\Users\nikit\Desktop\server\App\Application\Settings\Settings.json";
        public const string WRONG_PARAMETRS = "The wrong number of parameters";
        public const string NEED_LOGIN = "You need to be logged in to use this command";
        public const string WRONG_ROOM = "Room not found";
        public const string WRONG_PLAYER = "Player not found";
        public const string WRONG_ACCOUNT = "Account not found";
        public const uint DELETE_ROOM_TIME_MS = 30000;
        public const uint CHECK_LAST_PING_TIME_MS = 5000;
        public const uint CLEAR_HISTORY_TIME_MS = 10000;

        public static int GetParam(Command comm, int position)
        {
            return Convert.ToInt32(comm.parametrs[position]);
        }

        public static int Randomizer(int from, int to)
        {
            Random rnd = new Random();
            return rnd.Next(from, to);
        }

        public static class Fmt
        {
            public static string ToReplyStr(List<Account> accounts)
            {
                string toReply = "Active clients now: ";
                foreach (var item in accounts)
                    toReply += $"\n-----------------------------" +
                        $"\nId: {item.id}" +
                        $"\nLoggin: {item.loggin}";
                if (toReply == null)
                    return "";
                else
                    return toReply;
            }

            public static string ToReplyStr(List<Poll> polls)
            {
                string toReply = "Polls info: ";
                int id = 0;
                foreach (var poll in polls)
                {
                    toReply += $"\n-----------------------------" +
                        $"\nName: {poll.title}" +
                        $"\nId: {id}" +
                        $"\nStatus: {poll.status}" +
                        $"\nVotes amount: {poll.votes}" +
                        $"\nVotes: ";
                    if (poll.votesAmount.Count != 0)
                    {
                        foreach (var vote in poll.votesAmount)
                            toReply += $"{vote.vote} ";
                    }
                    else
                    {
                        toReply += "No one has voted yet";
                    }
                    id++;
                }
                if (toReply == null)
                    return "";
                else
                    return toReply;
            }

            public static void ConvertToCommand(Message comm)
            {
                string[] temp = comm.Text.Split(' ');
                if(temp.Length != 0)
                {                  
                    if (temp.Length == 1)
                    {
                        comm.Text = "{" +
                            $"\"command\":\"{temp[0]}\", \"parametrs\":[]" +
                            "}";
                    }
                    else
                    {
                        comm.Text = "{" +
                            $"\"command\":\"{temp[0]}\"," +
                            $"\"parametrs\":[";

                        for (int i = 1; i < temp.Length; i++)
                            comm.Text += $"\"{temp[i]}\",";

                        comm.Text += "]}" ;
                    }
                }
            }
        }
    }
}

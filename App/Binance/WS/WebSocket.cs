﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Core.FilesDatabase;
using App.Core;

namespace Server.Binance.WS
{
    public class WebSocket
    {
        private static ClientWebSocket webSocket = null;

        public WebSocket(string uri)
        {
            FileDB.CreateFile(Tables.TABLE_BINANCE_DATA);
            Connect(uri);
        }

        public static async void Connect(string uri)
        {
            try
            {
                webSocket = new ClientWebSocket();
                Logger.GetLog().Info("Connect to: " + Utlis.URI_BINANCE_DEMO);
                await webSocket.ConnectAsync(new Uri(uri), CancellationToken.None);
                await Task.WhenAll(Receive(webSocket));
            }
            catch (Exception ex)
            {
                Console.WriteLine("WebSock: {0}", ex);
            }
            finally
            {
                if (webSocket != null)
                {
                    webSocket.Dispose();
                    Logger.GetLog().Info("WebSocket has been disposed");
                }

                Logger.GetLog().Info("WebSocket closed.");
            }
        }

        private static async Task Send(ClientWebSocket webSocket, string message)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(message); ;

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, false, CancellationToken.None);
            Logger.GetLog().Info(Encoding.ASCII.GetString(buffer));
            await Receive(webSocket);
        }

        private static async Task Receive(ClientWebSocket webSocket)
        {
            byte[] bytes = new byte[4096];

            while (webSocket.State == WebSocketState.Open)
            {
                var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(bytes), CancellationToken.None); ;   
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    Logger.GetLog().Info("Received WebSocket close message");
                }
                else
                {
                    FileDB.WriteFile(Tables.TABLE_BINANCE_DATA, Encoding.ASCII.GetString(bytes, 0, result.Count));
                    Logger.GetLog().Info(Encoding.ASCII.GetString(bytes, 0, result.Count));
                }
            }
        }
    }
}

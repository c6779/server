﻿using App.Server.JsonReply;
using NetworkApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using App.Core;
using App.TelegramBot.Command;
using App.Server.Game;
using System.Threading;

namespace App.Server
{
    public static class RequestMethods
    {
        private static void HandleReply(Reply reply, string returned_string)
        {
            reply.Message = returned_string;
            Logger.GetLog().Info(returned_string);
        }

        private static bool IsValidLoggin(Reply reply, Command comm) 
        {
            if (reply.Payload.From == Payload.Types.From.Console)
            {
                if (!State.GetInst().activeAccounts.Contains(Utlis.GetParam(comm, comm.parametrs.Count - 1)))
                {
                    HandleReply(reply, Utlis.NEED_LOGIN);
                    return true;
                }
            }
            return false;
        }

        private static bool IsValidParams(Reply reply, Command comm, int paramCount)
        {
            if (comm.parametrs.Count != paramCount)
            {
                HandleReply(reply, Utlis.WRONG_PARAMETRS);
                return true;
            }
            return false;
        }

        private static Account GetAcc(Reply reply, Command comm)
        {
            if (reply.Payload.From == Payload.Types.From.Console)
                return State.GetInst().GetAccount(Utlis.GetParam(comm, comm.parametrs.Count - 1));
            else
                return State.GetInst().GetAccount(comm.parametrs[comm.parametrs.Count - 1]);
        }

        public static void CreatePolls(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 3))
                return;
            if (IsValidLoggin(reply, comm))
                return;

            else
            {
                State.GetInst().CreatePoll(comm.parametrs[0], Utlis.GetParam(comm, 1));
                HandleReply(reply, "Poll has been created");
            }
        }

        public static void Vote(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 3))
                return;
            if (IsValidLoggin(reply, comm))
                return;

            else
            {
                State.GetInst().MakeVote(Utlis.GetParam(comm, 0), Convert.ToBoolean(comm.parametrs[1]));
                HandleReply(reply, "Voting was successful");
            }
        }

        public static void Polls(Reply reply)
        {
            if (State.GetInst().polls.Count == 0)
            {
                HandleReply(reply, "Have not created a polls");
            }
            else
            {
                reply.Message = JsonConvert.SerializeObject(State.GetInst().polls);
                Logger.GetLog().Info("Geting info about polls");
            }
        }

        public static bool ActiveUsers(Reply reply)
        {
            if (State.GetInst().activeAccounts.Count == 0)
            {
                HandleReply(reply, "Have no active accounts");
                return false;
            }

            List<Account> info_about_active_users = new List<Account>();

            foreach (var id in State.GetInst().activeAccounts)
                info_about_active_users.Add(State.GetInst().GetAccount(id));

            reply.Message = JsonConvert.SerializeObject(info_about_active_users);
            Logger.GetLog().Info("Geting info about active users");
            return true;
        }

        public static void Registration(Reply reply, Command comm)
        {
            if(reply.Payload.From == Payload.Types.From.Console)
            {
                if(IsValidParams(reply, comm, 3))
                    return;
            }

            if (State.GetInst().GetAccount(comm.parametrs[0]) != null)
            {
                HandleReply(reply, "An account with the same name already exists");
            }
            else
            {
                if(reply.Payload.From == Payload.Types.From.Tgbot)
                    State.GetInst().Registration(comm.parametrs[0], " ", true);
                else
                    State.GetInst().Registration(comm.parametrs[0], comm.parametrs[1], false);
                HandleReply(reply, "Successfully registered");
            }
        }

        public static void Loggin(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 3))
                return;

            var acc = State.GetInst().GetAccount(comm.parametrs[0]);

            if (acc == null)
            {
                HandleReply(reply, Utlis.WRONG_ACCOUNT);
                return;
            }
            if (acc.password != comm.parametrs[1])
            {
                HandleReply(reply, "Wrong login or password");
                return;
            }
            if (State.GetInst().activeAccounts.Contains(Utlis.GetParam(comm, comm.parametrs.Count - 1)))
            {
                HandleReply(reply, "Already logged in");
                return;
            }

            State.GetInst().activeAccounts.Add(acc.id);
            reply.Message = Convert.ToString(acc.id);
            Logger.GetLog().Info("Authorization was successful");
        }

        public static void Pong(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            int key = Utlis.GetParam(comm, comm.parametrs.Count - 1);

            if (!State.GetInst().activeAccounts.Contains(key))
            {   
                HandleReply(reply, "Client is not active");
            }
            else
            {
                var timestamp = Convert.ToInt32(new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());
                Account acc = State.GetInst().GetAccount(Utlis.GetParam(comm, 0));
                if (State.GetInst().lastPing.ContainsKey(key))
                {
                    State.GetInst().lastPing[key] = timestamp;
                    acc.lastPing = timestamp;
                }
                else
                {
                    State.GetInst().lastPing.Add(key, timestamp);
                    acc.lastPing = timestamp;
                }
                State.GetInst().Update(acc);
                HandleReply(reply, "Client is active");
            }
        }

        public static void GetBinanceData(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            if (State.GetInst().ReadBinanceData() == "")
            {
                HandleReply(reply, "File is empty");
            }
            else
            {
                reply.Message = State.GetInst().ReadBinanceData();
                Logger.GetLog().Info("Show binance data");
            }
        }

        public static void Game(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            reply.Message = CommandDescr.COMMAND_FOR_GAME;
            Logger.GetLog().Info("Send game commands");
        }

        public static void ArenaCommands(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            reply.Message = CommandDescr.ARENA_COMMAND;
            Logger.GetLog().Info("Send arena commands");
        }

        public static void Rooms(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            reply.Message = CommandDescr.Rooms(State.GetInst().GetRooms());
            Logger.GetLog().Info("Send rooms list");
        }

        public static void CreateRoom(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            if (State.GetInst().GetActiveRoomCount() == 20)
            {
                HandleReply(reply, "The number of rooms cannot exceed 20");
                return;
            }

            Arena.CreateRoom();
            HandleReply(reply, "Room has been created");
            return;
        }

        public static void RemoveRoom(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 2))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            Room room = State.GetInst().GetRoom(Utlis.GetParam(comm, 0));
            State.GetInst().RemoveRoom(room);
            HandleReply(reply, "Room has been deleted");
            return;
        }

        public static void EnterRoom(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 2))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            Account acc = GetAcc(reply, comm);

            if (acc == null)
            {
                HandleReply(reply, Utlis.WRONG_ACCOUNT);
                return;
            }

            Player player = State.GetInst().GetPlayer(acc.loggin);
            if (player == null)
            {
                HandleReply(reply, Utlis.WRONG_PLAYER);
                return;
            }

            Room room = State.GetInst().GetRoom(Utlis.GetParam(comm, 0));
            if (room == null)
            {
                HandleReply(reply, Utlis.WRONG_ROOM);
                return;
            }

            if (room.status == Room.STATUS.END)
            {
                HandleReply(reply, "Room have END status");
                return;
            }

            room.AddPlayer(player);
            if (room.PlayerCount() == 2)
            {
                Arena.Start(room);
            }
            else
            {
                while(room.status != Room.STATUS.END)
                    Thread.Sleep(500);
            }

            reply.Message = JsonConvert.SerializeObject(room.gameHistory);
            room.amountSendingGamedHistory += 1;
            if (room.amountSendingGamedHistory == 2)
                room.gameHistory.Clear();
            Logger.GetLog().Info("Send game history");
        }

        public static void Profile(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;


            Account acc = GetAcc(reply, comm);

            if (acc == null)
            {
                HandleReply(reply, Utlis.WRONG_ACCOUNT);
                return;
            }

            Player player = State.GetInst().GetPlayer(acc.loggin);
            if (player == null)
            {
                HandleReply(reply, Utlis.WRONG_PLAYER);
                return;
            }

            reply.Message = CommandDescr.ShowProfile(player);
            Logger.GetLog().Info("Send profile info");
        }

        public static void Shop(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 1))
                return;

            if (IsValidLoggin(reply, comm))
                return;

            reply.Message = CommandDescr.Shop();
            Logger.GetLog().Info("Show shop");
        }

        public static void Buy(Reply reply, Command comm)
        {
            if (IsValidParams(reply, comm, 2))
                return;
                
            if (IsValidLoggin(reply, comm))
                return;

            Account acc = GetAcc(reply, comm);
            if (acc == null)
            {
                HandleReply(reply, Utlis.WRONG_ACCOUNT);
                return;
            }

            Player player = State.GetInst().GetPlayer(acc.loggin);
            if (player == null)
            {
                HandleReply(reply, Utlis.WRONG_PLAYER);
                return;
            }

            if (Server.Game.Shop.Buy(ref player, Utlis.GetParam(comm, 0)))
                HandleReply(reply, "Purchase completed successfully");
            else
                HandleReply(reply, "You don't have enough money");
        }

        public static void AnotherCommand(Reply reply, Command comm)
        {
            reply.Message = "Command:  " + comm.command + " " + "Parametrs: " + string.Join(", ", comm.parametrs.ToArray());
            Logger.GetLog().Info($"Server returns a message: " + reply.Message);
        }
    }
}

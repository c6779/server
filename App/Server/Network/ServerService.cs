﻿using Grpc.Core;
using NetworkApi;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Server.Binance.WS;
using App.Server;
using App.Server.JsonReply;
using App.Core;
using App.Server.Network;

namespace App.Src.Network
{
    internal class ServerService : Greeter.GreeterBase
    {
        private ReplyWrap replyWrap;
        public WebSocket webSocket;

        public override Task<Reply> Answer(HelloRequest request, ServerCallContext context)
        {
            Logger.GetLog().Info($"Server get message: {request.Command}");
            replyWrap = new ReplyWrap();
            var comm = JsonConvert.DeserializeObject<Command>(request.Command);

            if (comm.command == "create_poll")
                RequestMethods.CreatePolls(replyWrap.reply, comm);
            else if (comm.command == "ping")
                RequestMethods.Pong(replyWrap.reply, comm);
            else if (comm.command == "vote")
                RequestMethods.Vote(replyWrap.reply, comm);
            else if (comm.command == "polls")
                RequestMethods.Polls(replyWrap.reply);
            else if (comm.command == "registration")
                RequestMethods.Registration(replyWrap.reply, comm);
            else if (comm.command == "loggin")
                RequestMethods.Loggin(replyWrap.reply, comm);
            else if (comm.command == "active_users")
                RequestMethods.ActiveUsers(replyWrap.reply);
            else if (comm.command == "get_binance_data")
                RequestMethods.GetBinanceData(replyWrap.reply, comm);
            else if (comm.command == "game")
                RequestMethods.Game(replyWrap.reply, comm);
            else if (comm.command == "arena")
                RequestMethods.ArenaCommands(replyWrap.reply, comm);
            else if (comm.command == "profile")
                RequestMethods.Profile(replyWrap.reply, comm);
            else if (comm.command == "rooms")
                RequestMethods.Rooms(replyWrap.reply, comm);
            else if (comm.command == "create_room")
                RequestMethods.CreateRoom(replyWrap.reply, comm);
            else if (comm.command == "remove_room")
                RequestMethods.RemoveRoom(replyWrap.reply, comm);
            else if (comm.command == "enter_room")
                RequestMethods.EnterRoom(replyWrap.reply, comm);
            else if (comm.command == "shop")
                RequestMethods.Shop(replyWrap.reply, comm);
            else if (comm.command == "buy")
                RequestMethods.Buy(replyWrap.reply, comm);
            else
                RequestMethods.AnotherCommand(replyWrap.reply, comm);
            return Task.FromResult(replyWrap.reply);
        }
    }
}

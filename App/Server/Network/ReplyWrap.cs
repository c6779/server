﻿using NetworkApi;

namespace App.Server.Network
{
    public class ReplyWrap
    {
        public Reply reply;
        
        public ReplyWrap() 
        {
            reply = new Reply();
            reply.Payload = new Payload();
            reply.Payload.From = Payload.Types.From.Console;
        }

        public ReplyWrap(Payload.Types.From from)
        {
            reply = new Reply();
            reply.Payload = new Payload();
            reply.Payload.From = from;
        }
    }
}

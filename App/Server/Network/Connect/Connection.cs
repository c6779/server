﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App;
using Grpc.Core;
using Grpc.Net.Client;
using App.Core;

namespace Server.Src.Network.Connect
{
    internal class Connection
    {
        private string _ip_v4;
        private int _port;
        private string _address;
        public GrpcChannel channel;

        public Connection()
        {
            _ip_v4 = "localhost";
            _port = 5000;
            _address = $"{_ip_v4}:{_port}";
            channel = GrpcChannel.ForAddress("http://" + _address);
            Logger.GetLog().Info($"Connect succec: " + _address);
        }

        public Connection(string ip_v4, int port)
        {
            _ip_v4 = ip_v4;
            _port = port;
            _address = $"{_ip_v4}:{_port}";
            channel = GrpcChannel.ForAddress("http://" + _address);
            Logger.GetLog().Info($"Connect succec: " + _address);
        }
    }
}

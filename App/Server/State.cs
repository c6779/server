﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;
using System;
using App.Server.JsonReply;
using App.Core.FilesDatabase;
using App.Core;
using App.Server.Game;
using System.Collections;

namespace App.Server
{
    public class State
    {
        public List<Poll> polls;
        public List<Account> accounts;
        public List<int> activeAccounts;
        public List<Player> players;
        public Hashtable playersItems;
        private Hashtable _statistics;
        private List<Room> _rooms;
        private static int _id;
        private Timer _timer;
        public Dictionary<int, int> lastPing;
        private static State _instance;

        public State()
        {
            polls = new List<Poll>();
            accounts = new List<Account>();
            lastPing = new Dictionary<int, int>();
            players = new List<Player>();
            playersItems = new Hashtable();
            _statistics = new Hashtable();
            
            FileDB.CreateFile(Tables.TABLE_ACCOUNT_NAME);
            FileDB.CreateFile(Tables.TABLE_POLL_NAME);
            FileDB.CreateFile(Tables.TABLE_ACTIVE_ACCOUNTS);
            FileDB.CreateFile(Tables.TABLE_ID);
            FileDB.CreateFile(Tables.TABLE_PLAYERS);
            FileDB.CreateFile(Tables.TABLE_ROOMS);
            FileDB.CreateFile(Tables.TABLE_PLAYERS_ITEMS);
            FileDB.CreateFile(Tables.TABLE_PLAERS_STATISTICS);

            string fileToReadPoll = FileDB.ReadFile(Tables.TABLE_POLL_NAME);
            string fileToReadAccount = FileDB.ReadFile(Tables.TABLE_ACCOUNT_NAME);
            string fileToReadID = FileDB.ReadFile(Tables.TABLE_ID);
            string fileToReadActiveAcc = FileDB.ReadFile(Tables.TABLE_ACTIVE_ACCOUNTS);
            string fileToReadPlayers = FileDB.ReadFile(Tables.TABLE_PLAYERS);
            string fileToReadPlayersItems = FileDB.ReadFile(Tables.TABLE_PLAYERS_ITEMS);
            string fileToReadStatistics = FileDB.ReadFile(Tables.TABLE_PLAERS_STATISTICS);

            polls = JsonConvert.DeserializeObject<List<Poll>>(fileToReadPoll);
            accounts = JsonConvert.DeserializeObject<List<Account>>(fileToReadAccount);
            activeAccounts = JsonConvert.DeserializeObject<List<int>>(fileToReadActiveAcc);
            players = JsonConvert.DeserializeObject<List<Player>>(fileToReadPlayers);
            playersItems = JsonConvert.DeserializeObject<Hashtable>(fileToReadPlayersItems);
            _statistics = JsonConvert.DeserializeObject<Hashtable>(fileToReadStatistics);

            if (polls == null)
                polls = new List<Poll>();
            if (accounts == null)
                accounts = new List<Account>();
            if (activeAccounts == null)
                activeAccounts = new List<int>();
            if (players == null)
                players = new List<Player>();
            if (_rooms == null)
                _rooms = new List<Room>();
            if (playersItems == null)
                playersItems = new Hashtable();
            if (_statistics == null)
                _statistics = new Hashtable();
            if (fileToReadID == "")
                _id = 0;
            else
                _id = JsonConvert.DeserializeObject<int>(fileToReadID);

            TimerCallback tm = new TimerCallback(CheckLastPing);
            //_timer = new Timer(tm, null, 0, Utlis.CHECK_LAST_PING_TIME_MS);
        }

        public static State GetInst()
        {
            if (_instance == null)
                _instance = new State();
            return _instance;
        }

        public void SetPlayersItems()
        {
            if (accounts.Count != 0 && playersItems.Count != 0)
                foreach (var player in players)
                    player.AddItem();
        }

        public Statistics GetStatistic(int id)
        {
            if (_statistics.ContainsKey(id.ToString()))
            {
                var statistic = _statistics[id.ToString()];
                return JsonConvert.DeserializeObject<Statistics>(statistic.ToString());
            }
            return null;
        }

        public IOitemJson GetItemsList(int id)
        {
            if (playersItems.ContainsKey(id.ToString()))
            {
                var player = playersItems[id.ToString()];
                return JsonConvert.DeserializeObject<IOitemJson>(player.ToString());
            }
            return null;
        }

        public void Update(Statistics statistics)
        {
            if (_statistics.ContainsKey(statistics.accId.ToString()))
                _statistics.Remove(statistics.accId.ToString());
            _statistics.Add(statistics.accId, statistics);
            FileDB.WriteFile(Tables.TABLE_PLAERS_STATISTICS, JsonConvert.SerializeObject(_statistics));
        }

        public void Update(IOitemJson listItems)
        {
            if (playersItems.ContainsKey(listItems.id.ToString()))
                playersItems.Remove(listItems.id.ToString());
            playersItems.Add(listItems.id, listItems);
            FileDB.WriteFile(Tables.TABLE_PLAYERS_ITEMS, JsonConvert.SerializeObject(playersItems));
        }

        public void Update(Room room)
        {
            _rooms.Add(room);
            FileDB.WriteFile(Tables.TABLE_ROOMS, JsonConvert.SerializeObject(_rooms));
        }
        public void Update(Account account)
        {
            accounts.Add(account);
            FileDB.WriteFile(Tables.TABLE_ACCOUNT_NAME, JsonConvert.SerializeObject(accounts));
        }

        public void Update(string fileName)
        {
            FileDB.WriteFile(fileName, JsonConvert.SerializeObject(players));
        }

        public void RemoveRoom(Room room)
        {
            _rooms.Remove(room);
            FileDB.WriteFile(Tables.TABLE_ROOMS, JsonConvert.SerializeObject(_rooms));
        }

        public Room GetRoom(int roomId)
        {
            for (int i = 0; i < _rooms.Count; i++)
                if (_rooms[i].id == roomId)
                    return _rooms[i];
            return null;
        }

        public int GetActiveRoomCount()
        {
            int count = 0;
            foreach (var room in _rooms)
                if (room.status == Room.STATUS.WAIT_PLSYERS) 
                    count++;
            return count;
        }

        public List<Room> GetRooms()
        {
            return _rooms;
        }

        public void Registration(string loggin, string password, bool isTgAcc)
        {
            accounts.Add(new Account(loggin, password, GetId(), isTgAcc));
            players.Add(new Player(accounts[accounts.Count - 1]));
            playersItems.Add(accounts[accounts.Count - 1].id, new IOitemJson(accounts[accounts.Count - 1].id));
            _statistics.Add(accounts[accounts.Count -1].id, new Statistics(accounts[accounts.Count - 1].id));
            FileDB.WriteFile(Tables.TABLE_PLAYERS, JsonConvert.SerializeObject(players));
            FileDB.WriteFile(Tables.TABLE_ACCOUNT_NAME, JsonConvert.SerializeObject(accounts));
            FileDB.WriteFile(Tables.TABLE_PLAERS_STATISTICS, JsonConvert.SerializeObject(_statistics));
            FileDB.WriteFile(Tables.TABLE_PLAYERS_ITEMS, JsonConvert.SerializeObject(playersItems));
        }

        public Account GetAccount(string loggin)
        {
            for (int i = 0; i < accounts.Count; i++)
                if (accounts[i].loggin == loggin)
                    return accounts[i];
            return null;
        }

        public Account GetAccount(int id)
        {
            for (int i = 0; i < accounts.Count; i++)
                if (accounts[i].id == id)
                    return accounts[i];
            return null;
        }

        public Player GetPlayer(string loggin)
        {
            for (int i = 0; i < players.Count; i++)
                if (players[i].accLogin == loggin)
                    return players[i];
            return null;
        }

        public int GetId()
        {
            FileDB.WriteFile(Tables.TABLE_ID, JsonConvert.SerializeObject(++_id));
            return _id;
        }

        public void CreatePoll(string title, int votes)
        {
            polls.Add(new Poll(title, votes));
            FileDB.WriteFile(Tables.TABLE_POLL_NAME, JsonConvert.SerializeObject(polls));
        }

        public void MakeVote(int poll_id, bool vote)
        {
            if (polls.Count > poll_id - 1 && polls[poll_id].status != STATUS.end)
            {
                polls[poll_id].votesAmount.Add(new Vote(poll_id, vote));
            }
            if (polls[poll_id].votesAmount.Count >= polls[poll_id].votes)
            {
                polls[poll_id].status = STATUS.end;
            }
            FileDB.WriteFile(Tables.TABLE_POLL_NAME, JsonConvert.SerializeObject(polls));
        }

        public void CheckLastPing(object obj)
        {
            int present_time = Convert.ToInt32(new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());

            foreach (var item in lastPing)
            {
                if (item.Value + 15 <= present_time)
                {
                    lastPing.Remove(item.Key);
                    activeAccounts.Remove(item.Key);
                    FileDB.WriteFile(Tables.TABLE_ACTIVE_ACCOUNTS, JsonConvert.SerializeObject(activeAccounts));
                    Logger.GetLog().Info($"Bad ping id {item.Key}");
                }
                else
                {
                    FileDB.WriteFile(Tables.TABLE_ACTIVE_ACCOUNTS, JsonConvert.SerializeObject(activeAccounts));
                    Logger.GetLog().Info($"Ping id {item.Key} is normal");
                }
            }
        }

        public string ReadBinanceData()
        {
            string fileToReadBinanceData = FileDB.ReadFile(Tables.TABLE_BINANCE_DATA);

            if (fileToReadBinanceData == "")
                return "";
            else
                return fileToReadBinanceData;
        }
    }
}

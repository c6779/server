﻿using App.Core;
using App.Server.JsonReply;
using System.Collections.Generic;
using App.Core.FilesDatabase;

namespace App.Server.Game
{
    public class Player
    {
        public string accLogin;
        public int money;
        public int health;
        public int damage;
        private List<IOitem> _items;

        public Player() 
        {
            _items = new List<IOitem>();
        }

        public Player(Account account)
        {
            accLogin = account.loggin;
            health = 100;
            damage = 10;
            _items = new List<IOitem>();
        }

        private int CalcDamage()
        {
            return damage + Utlis.Randomizer(10, 20);
        }

        public int CalcHelth()
        {
            int calcHP = 100;

            if (_items.Count == 0)
                return calcHP;

            foreach (var item in _items)
                if (item.iType == ItemType.CLOTHES)
                    calcHP += ((IClothes)item).health;
                else if (item.iType == ItemType.RUNES)
                    calcHP += ((IRune)item).health;

            return calcHP;
        }

        public int Atack(Player player) 
        {
            int calcDamage = CalcDamage();
            player.health -= calcDamage;
            if (player.health < 0)
                player.health = 0;
            return calcDamage;
        }

        public bool IsDead()
        {
            if (health == 0 ) 
                return true;
            return false;
        }

        public List<IOitem> GetItems()
        {
            return _items;
        }

        public void AddItem()
        {
            var items = State.GetInst().GetItemsList(State.GetInst().GetAccount(accLogin).id);
            if (items == null)
                return;

            if (items.weapons.Count != 0)
                foreach (var weapon in items.weapons)
                    _items.Add(weapon);
            if (items.clothes.Count != 0)
                foreach (var clothes in items.clothes)
                    _items.Add(clothes);
            if (items.runes.Count != 0)
                foreach (var rune in items.runes)
                    _items.Add(rune);
        }

        public void AddItem(IOitem item)
        {
            _items.Add(item);

            IOitemJson listItems = State.GetInst().GetItemsList(State.GetInst().GetAccount(accLogin).id);

            if (item.iType == ItemType.WEAPON)
            {
                listItems.weapons.Add((IWeapon)item);
                damage += ((IWeapon)item).damage;
            }
            else if (item.iType == ItemType.CLOTHES)
            {
                listItems.clothes.Add((IClothes)item);
                health += ((IRune)item).health;
            }
            else if (item.iType == ItemType.RUNES)
            {
                listItems.runes.Add((IRune)item);
                damage += ((IRune)item).damage;
                health += ((IRune)item).health;
            }

            State.GetInst().Update(listItems);
            State.GetInst().Update(Tables.TABLE_PLAYERS);
        }
    }
}

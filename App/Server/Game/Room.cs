﻿using App.Core;
using App.Server.JsonReply;
using System;
using System.Collections.Generic;
using System.Threading;

namespace App.Server.Game
{
    public class Room
    {
        public enum STATUS
        {
            WAIT_PLSYERS,
            PLAY,
            END
        }

        public int id;
        public int reward;
        public ValueTuple<Player, Player> players;
        public STATUS status;
        public List<string> gameHistory;
        public int winnerID;
        private Timer _closeRoom;
        public int amountSendingGamedHistory;

        public Room() { }

        public Room(int id) 
        {
            amountSendingGamedHistory = 0;
            this.id = id;
            players = (new Player(), new Player());
            status = STATUS.WAIT_PLSYERS;
            gameHistory = new List<string>();
            reward = Utlis.Randomizer(1, 100);
            var autoEvent = new AutoResetEvent(true);
            TimerCallback tm = new TimerCallback(CloseRoom);
            _closeRoom = new Timer(tm, autoEvent, Utlis.DELETE_ROOM_TIME_MS, 0);
        }   

        private void CloseRoom(object state)
        {
            status = STATUS.END;
            State.GetInst().Update(State.GetInst().GetRoom(id));
        }

        private int AddMoney(Player player)
        {
            if (player.GetItems().Count != 0)
                foreach (var item in player.GetItems())
                    if (item.iType == ItemType.RUNES) 
                        reward += (reward / 100) * ((IRune)item).reward;
            return reward;
        }

        private void SetFullHp()
        {
            players.Item1.health = players.Item1.CalcHelth();
            players.Item2.health = players.Item2.CalcHelth();
        }

        private void AddStatisticAndReward(Player loser, Player winner)
        {
            Account winnerAcc = State.GetInst().GetAccount(winner.accLogin);
            Account loserAcc = State.GetInst().GetAccount(loser.accLogin);
            Statistics winnerStat = State.GetInst().GetStatistic(winnerAcc.id);
            Statistics loserStat = State.GetInst().GetStatistic(loserAcc.id);
            winnerStat.wins++;
            loserStat.losses++;
            winner.money += AddMoney(winner);
            State.GetInst().Update(winnerStat);
            State.GetInst().Update(loserStat);
        }

        private void ProccesEndPvP(Player winner, Player loser)
        {
            AddStatisticAndReward(loser, winner);
            gameHistory.Add($"{winner.accLogin} - WIN! - Reward {reward} money.");
            winnerID = State.GetInst().GetAccount(winner.accLogin).id;
            SetFullHp();
        }

        private void ClearHistory(object state)
        {
            gameHistory.Clear();
            State.GetInst().Update(State.GetInst().GetRoom(id));
        }

        public int PlayerCount()
        {
            int count = 0;
            if (players.Item1.accLogin != null)
                count++;
            if (players.Item2.accLogin != null)
                count++;
            return count;
        }

        public void AddPlayer(Player player)
        {
            if (players.Item1.accLogin == null)
            {
                players.Item1 = player;
                return;
            }
            players.Item2 = player;
        }

        public void PvP()
        {
            gameHistory.Add($"Start fight: {players.Item1.accLogin}({players.Item1.health}) vs {players.Item2.accLogin}({players.Item2.health})");

            while(true)
            {
                gameHistory.Add($"{players.Item1.accLogin}({players.Item1.health}) damage {players.Item1.Atack(players.Item2)} to " +
                    $"{players.Item2.accLogin}({players.Item2.health})");
                if (players.Item2.IsDead())
                {
                    ProccesEndPvP(players.Item1, players.Item2);
                    break;
                }

                gameHistory.Add($"{players.Item2.accLogin}({players.Item2.health}) damage  {players.Item2.Atack(players.Item1)} to " +
                    $"{players.Item1.accLogin}({players.Item1.health})");
                if (players.Item1.IsDead())
                {
                    ProccesEndPvP(players.Item2, players.Item1);
                    break;
                }
            }
            
        }
    }
}

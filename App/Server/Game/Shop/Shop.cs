﻿using System.Collections;
using System.Collections.Generic;
using App.Core.FilesDatabase;

namespace App.Server.Game
{
    public static class Shop
    {
        private static Hashtable _items = new Hashtable();

        public static Hashtable GetItems() => _items;

        public static void AddItems(List<IWeapon> items)
        {
            if (_items != null)
                foreach (var item in items)
                    _items.Add(item.id, item);
        }

        public static void AddItems(List<IClothes> items)
        {
            if (_items != null)
                foreach (var item in items)
                    _items.Add(item.id, item);
        }

        public static void AddItems(List<IRune> items)
        {
            if (_items != null)
                foreach (var item in items)
                    _items.Add(item.id, item);
        }

        public static List<IWeapon> GetWeapons()
        {
            List<IWeapon> weapons = new List<IWeapon>();
            foreach (var key in _items.Keys)
                if (_items[key] is IWeapon)
                    weapons.Add((IWeapon)_items[key]);
            return weapons;
        }

        public static List<IClothes> GetClothes()
        {
            List<IClothes> clothes = new List<IClothes>();
            foreach (var key in _items.Keys)
                if (_items[key] is IClothes)
                    clothes.Add((IClothes)_items[key]);
            return clothes;
        }

        public static List<IRune> GetRunes()
        {
            List<IRune> runes = new List<IRune>();;
            foreach (var key in _items.Keys)
                if (_items[key] is IRune)
                    runes.Add((IRune)_items[key]);
            return runes;   
        }

        public static bool Buy(ref Player player, int id)
        {
            IOitem item = (IOitem)_items[id];
            if (player.money >= item.price)
            {
                player.AddItem(item);
                player.money -= item.price;
                State.GetInst().Update(Tables.TABLE_PLAYERS);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

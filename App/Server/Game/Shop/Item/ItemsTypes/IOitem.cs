﻿namespace App.Server.Game
{
    public abstract class IOitem
    {
        public int id;
        public string name;
        public int price;
        public ItemType iType;
    }
}

﻿namespace App.Server.Game
{
    public enum ItemType
    {
        WEAPON,
        CLOTHES,
        RUNES
    }

    public enum ClothesType
    {
        TSHIRT,
        SHORTS,
        BRASERS,
        BOOTS
    }

    public enum RunesType
    {
        WEALTH,
        WAR,
        LOVE,
        EQUAL
    }

    public enum WeaponType
    {
        STICK,
        SWORD
    }
}

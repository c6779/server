﻿namespace App.Server.Game
{
    public class IRune : IOitem
    {
        public int health;
        public int damage;
        public int reward;
        public RunesType rType;
    }
}

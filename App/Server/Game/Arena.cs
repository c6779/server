﻿using App.Core.FilesDatabase;
using System.Collections.Generic;

namespace App.Server.Game
{
    public static class Arena
    {
        public static void CreateRoom()
        {
            State.GetInst().Update(new Room(State.GetInst().GetId()));
        }

        public static void Start(Room room)
        {
            room.status = Room.STATUS.PLAY;
            State.GetInst().Update(room);
            room.PvP();
            room.status = Room.STATUS.END;
        }
    }
}

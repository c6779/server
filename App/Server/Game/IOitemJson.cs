﻿using App.Server.Game;
using System.Collections.Generic;

namespace App.Server.Game
{
    public class IOitemJson
    {
        public int id;
        public List<IWeapon> weapons;
        public List<IClothes> clothes;
        public List<IRune> runes;

        public IOitemJson()  { }

        public IOitemJson(int id) 
        {
            weapons = new List<IWeapon>();
            clothes = new List<IClothes>();
            runes = new List<IRune>();
            this.id = id;
        }
    }
}

﻿namespace App.Server.Game
{
    public class Statistics
    {
        public int accId;
        public int wins;
        public int losses;

        public Statistics(int accId)
        {
            this.accId = accId;
            wins = 0;
            losses = 0;
        }
    }
}

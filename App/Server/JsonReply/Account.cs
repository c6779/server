﻿namespace App.Server.JsonReply
{
    public class Account
    {
        public string loggin;
        public string password;
        public int id;
        public int lastPing;
        public bool isTgAcc;

        public Account() { }

        public Account(string loggin, string password, int id, bool isTgAcc)
        {
            this.loggin = loggin;
            this.password = password;
            this.id = id;
            this.isTgAcc = isTgAcc;
        }
    }
}

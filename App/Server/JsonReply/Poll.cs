﻿using System.Collections.Generic;

namespace App.Server
{
    public enum STATUS
    {
        progress,
        end
    }

    public class Poll
    {
        public string title;
        public STATUS status;
        public int votes;
        public List<Vote> votesAmount;

        public Poll(string title, int votes)
        {
            this.votes = votes;
            this.title = title;
            status = STATUS.progress;
            votesAmount = new List<Vote>();
        }
    }
}

﻿using Grpc.Core;
using System;
using System.Collections.Generic;

namespace App.Server
{
    public class Vote
    {
        public int pollId;
        public bool vote;

        public Vote(int pollId, bool vote)
        {
            this.pollId = pollId;
            this.vote = vote;
        }
    }
}

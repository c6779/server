﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Linq;
using App.Core.FilesDatabase;
using App.Core;
using App.Server.Game;
using App.Server;

namespace App.Application.Settings
{
    public class Settings
    {
        private Dictionary<string, string> _stickers = new Dictionary<string, string>();
        private Dictionary<string, string> _pictures = new Dictionary<string, string>();
        private string _token;

        public Settings()
        {
            InitSettings();
        }

        public string GetToken() => _token;

        public void InitSettings()
        {
            string json = FileDB.ReadFile(Utlis.SETTINGS_JSON);
            SettingsJson settings = JsonConvert.DeserializeObject<SettingsJson>(json);

            if (settings == null)
                throw new NotImplementedException("Settings.json doesnt exist");

            _token = settings.token;

            foreach (var sticker in settings.stickers)
                _stickers.Add(sticker.name, sticker.url);

            foreach (var picture in settings.pictures)
                _pictures.Add(picture.name, picture.url);

            Shop.AddItems(settings.weapons);
            Shop.AddItems(settings.clothes);
            Shop.AddItems(settings.runes);

            State.GetInst().SetPlayersItems();
        }

        public string Randomizer(Dictionary<string, string> dict)
        {
            Random rnd = new Random();
            int index = rnd.Next(dict.Count);
            KeyValuePair<string, string> pair = dict.ElementAt(index);
            return pair.Value;
        }

        public string GetRandomSticker()
        {
            return Randomizer(_stickers);
        }

        public string GetRandomPicture()
        {
            return Randomizer(_pictures);
        }
    }
}

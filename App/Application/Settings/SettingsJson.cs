﻿using System.Collections.Generic;
using App.Server.Game;
using App.TelegramBot.Preset;

namespace App.Application.Settings
{
    public class SettingsJson
    {
        public string token;
        public List<Preset> stickers;
        public List<Preset> pictures;
        public List<IWeapon> weapons;
        public List<IClothes> clothes;
        public List<IRune> runes;
    }
}

﻿using Server.Binance;
using System.Collections.Generic;
using System;
using Server.TelegramBot;
using System.Threading;
using Server.TelegramBot.Comm;
using App.Application.Settings;
using App.Core.FilesDatabase;
using App.Server;

namespace Server.App
{
    public class Application
    {
        private BinanceStream _binanceStream;
        private TgBot _tgBot;
        private Settings _settings;
        private Thread _thread;

        public Application()
        {
            State.GetInst();
            //_binanceStream = new BinanceStream();
            _settings = new Settings();
            _tgBot = new TgBot(_settings.GetToken(), ref _settings);
            _thread = new Thread(() =>
            {
                _tgBot.Start(_tgBot.client);
            });
            _thread.Start();
        }
    }
}

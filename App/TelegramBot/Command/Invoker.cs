﻿using Telegram.Bot.Types;
using Telegram.Bot;
using App.Application.Settings;
using App.Core;
using App.Server.JsonReply;

namespace Server.TelegramBot.Comm
{
    public class Invoker
    {
        private Message _command;
        private CommandExec _commExec;

        public Invoker(ref Settings settings, ref TelegramBotClient client)
        {
            _command = new Message();
            _commExec = new CommandExec(ref settings, ref client);
        }

        public void SetCommand(Message command)
        {
            _command = command;
        }

        public void Response()
        {
            Logger.GetLog().Info($"[TgBot] Incoming message: {_command.Text}");

            if(_command.Text != null)
            {
                _commExec.Execute(_command);
            }
        }
    }
}

﻿using Telegram.Bot.Types;
using Telegram.Bot;
using App.Server.JsonReply;
using App.Application.Settings;
using App.Core;
using Newtonsoft.Json;
using System.Diagnostics.Eventing.Reader;
using System.Threading;

namespace Server.TelegramBot.Comm
{
    public class CommandExec: ICommand
    {
        private Receiver _receiver;

        public CommandExec(ref Settings settings, ref TelegramBotClient client)
        {
            _receiver = new Receiver(ref settings, ref client);
        }

        public void Execute(Message message)
        {
            Utlis.Fmt.ConvertToCommand(message);
            var command = JsonConvert.DeserializeObject<Command>(message.Text);

            if (command.command.ToLower() == "/start")
                _receiver.Start(message, command);
            else if (command.command.ToLower() == "game")
                _receiver.Game(message);
            else if (command.command.ToLower() == "sticker")
                _receiver.Sticker(message);
            else if (command.command.ToLower() == "picture")
                _receiver.Picture(message);
            else if (command.command.ToLower() == "get_binance_data")
                _receiver.GetBinanceData(message);
            else if (command.command.ToLower() == "active_users")
                _receiver.ActiveUers(message);
            else if (command.command.ToLower() == "polls")
                _receiver.Polls(message);
            else if (command.command.ToLower() == "vote")
                _receiver.Vote(message, command);
            else if (command.command.ToLower() == "create_poll")
                _receiver.CreatePoll(message, command);
            else if (command.command.ToLower() == "arena")
                _receiver.ArenaCommands(message);
            else if (command.command.ToLower() == "profile")
                _receiver.Profile(message, command);
            else if (command.command.ToLower() == "rooms")
                _receiver.Rooms(message);
            else if (command.command.ToLower() == "create_room")
                _receiver.CreateRoom(message);
            else if (command.command.ToLower() == "remove_room")
                _receiver.RemoveRoom(message, command);
            else if (command.command.ToLower() == "enter_room")
                _receiver.EnterRoomAsync(message, command);
            else if (command.command.ToLower() == "shop")
                _receiver.Shop(message, command);
            else if (command.command.ToLower() == "buy")
                _receiver.Buy(message, command);
            else
                _receiver.Uncknown(message);
        }
    }
}

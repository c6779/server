﻿using Telegram.Bot.Types;
using Telegram.Bot;

namespace Server.TelegramBot.Comm
{
    public interface ICommand
    {
        void Execute(Message comm);
    }
}

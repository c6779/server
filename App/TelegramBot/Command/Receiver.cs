﻿using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types;
using Telegram.Bot;
using NetworkApi;
using App.Server;
using App.Application.Settings;
using App.Core;
using Newtonsoft.Json;
using System.Collections.Generic;
using App.Server.JsonReply;
using Poll = App.Server.Poll;
using App.Server.Network;
using App.TelegramBot.Command;
using App.Server.Game;
using System;
using System.Threading;

namespace Server.TelegramBot.Comm
{
    public class Receiver
    {
        private Settings _setting;
        private ITelegramBotClient _botClient;

        public Receiver(ref Settings setting, ref TelegramBotClient client)
        {
            _setting =  setting;
            _botClient = client;
        }

        private string CommandsDescription()
        {
            string toReply = "--------------------------Commands--------------------------\n" +
                   "Command: sticker\n" +
                   "Parametrs: -\n" +
                   "Description: displays a random sticker\n" +
                   "-----------------------------\n" +
                   "Command: picture\n" +
                   "Parametrs: -\n" +
                   "Description: displays a random picture\n" +
                   "-----------------------------\n" +
                   "Command: get_binance_data\n" +
                   "Parametrs: -\n" +
                   "Description: latest binance data\n" +
                   "-----------------------------\n" +
                   "Command: active_users\n" +
                   "Parametrs: -\n" +
                   "Descritpion: show info about active users;\n" +
                   "-----------------------------\n" +
                   "Command: polls\n" +
                   "Parametrs: -\n" +
                   "Description: show info about available votes;\n" +
                   "-----------------------------\n" +
                   "Command: game\n" +
                   "Parametrs: -\n" +
                   "Description: send game command\n" +
                   "-----------------------------\n" +
                   "Command: create_poll\n" +
                   "Parametrs: <poll_name> <vote_amount>\n" +
                   "Description: create new poll\n" +
                   "-----------------------------\n" +
                   "Command: vote\n" +
                   "Parametrs: <poll_id> <true|false>\n" +
                   "Description: vote for something";

            return toReply;
        }
        
        public void Start(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            command.parametrs.Add(message.From.Username);
            RequestMethods.Registration(replyWrap.reply, command);

            _botClient.SendTextMessageAsync(message.Chat.Id, CommandsDescription());
        }

        public void Sticker(Message comm)
        {
            _botClient.SendStickerAsync(chatId: comm.Chat.Id,
                sticker: _setting.GetRandomSticker(),
                cancellationToken: default);
        }

        public void Picture(Message comm)
        {
            _botClient.SendPhotoAsync(
                           chatId: comm.Chat.Id,
                           photo: _setting.GetRandomPicture(),
                           parseMode: ParseMode.Html,
                           cancellationToken: default);
        }

        public void GetBinanceData(Message comm)
        {
            _botClient.SendTextMessageAsync(comm.Chat.Id, "Last binance data: \n\n\t" + State.GetInst().ReadBinanceData());
        }

        public void ActiveUers(Message comm)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);

            if(!RequestMethods.ActiveUsers(replyWrap.reply))
            {
                _botClient.SendTextMessageAsync(comm.Chat.Id, replyWrap.reply.Message);
                return;
            }

            List<Account> active_users = JsonConvert.DeserializeObject<List<Account>>(replyWrap.reply.Message);
            _botClient.SendTextMessageAsync(comm.Chat.Id, Utlis.Fmt.ToReplyStr(active_users));
        }

        public void Polls(Message comm)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            RequestMethods.Polls(replyWrap.reply);
            List<Poll> polls = JsonConvert.DeserializeObject<List<Poll>>(replyWrap.reply.Message);

            _botClient.SendTextMessageAsync(comm.Chat.Id, Utlis.Fmt.ToReplyStr(polls));
        }

        public void Vote(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            RequestMethods.Vote(replyWrap.reply, command);

            _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
        }

        public void CreatePoll(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            RequestMethods.CreatePolls(replyWrap.reply, command);

            _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
        }

        public void Game(Message message)
        {
            _botClient.SendTextMessageAsync(message.Chat.Id, CommandDescr.COMMAND_FOR_GAME);
        }

        public void ArenaCommands(Message message)
        {
            _botClient.SendTextMessageAsync(message.Chat.Id, CommandDescr.ARENA_COMMAND);
        }

        public void Profile(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            command.parametrs.Add(message.From.Username);
            RequestMethods.Profile(replyWrap.reply, command);

            _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
        }

        public void Rooms (Message message)
        {
            _botClient.SendTextMessageAsync(message.Chat.Id, CommandDescr.Rooms(State.GetInst().GetRooms()));
        }

        public void CreateRoom(Message message)
        {
            Arena.CreateRoom();
            _botClient.SendTextMessageAsync(message.Chat.Id, "Room has been created");
        }

        public void RemoveRoom(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            Room room = State.GetInst().GetRoom(Convert.ToInt32(command.parametrs[0]));
            State.GetInst().RemoveRoom(room);

            _botClient.SendTextMessageAsync(message.Chat.Id, "Room has been deleted");
        }

        public void EnterRoom(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            command.parametrs.Add(message.From.Username);
            RequestMethods.EnterRoom(replyWrap.reply, command);
            if (replyWrap.reply.Message == null)
            {
                _botClient.SendTextMessageAsync(message.Chat.Id, "You can enter the room within 30 seconds");
            }
            else if (replyWrap.reply.Message == Utlis.NEED_LOGIN || replyWrap.reply.Message == Utlis.WRONG_ROOM ||
                        replyWrap.reply.Message == Utlis.WRONG_PLAYER || replyWrap.reply.Message == Utlis.WRONG_ACCOUNT 
                        || replyWrap.reply.Message == Utlis.WRONG_PARAMETRS)
            {
                _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
            }
            else
            {
                List<string> gameHistory = JsonConvert.DeserializeObject<List<string>>(replyWrap.reply.Message);
                foreach (var item in gameHistory)
                {
                    _botClient.SendTextMessageAsync(message.Chat.Id, item); ;
                    Thread.Sleep(200);
                }
            }
        }

        public void EnterRoomAsync(Message message, Command command)
        {
            Thread thread;
            thread = new Thread(() =>
            {
                EnterRoom(message, command);
            });
            thread.Start();
        }

        public void Shop(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            command.parametrs.Add(message.From.Username);
            RequestMethods.Shop(replyWrap.reply, command);
            _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
        }

        public void Buy(Message message, Command command)
        {
            ReplyWrap replyWrap = new ReplyWrap(Payload.Types.From.Tgbot);
            command.parametrs.Add(message.From.Username);
            RequestMethods.Buy(replyWrap.reply, command);
            _botClient.SendTextMessageAsync(message.Chat.Id, replyWrap.reply.Message);
        }

        public void Uncknown(Message message)
        {
            _botClient.SendTextMessageAsync(message.Chat.Id, "Uncknown command");
            Logger.GetLog().Info("[TgBot] Uncknown command");
        }
    }
}

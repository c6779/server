﻿using App.Server.Game;
using System.Collections.Generic;
using App.Server;

namespace App.TelegramBot.Command
{
    public static class CommandDescr
    {
        public const string COMMAND_FOR_GAME = "\n---------GameCommands--------\n\n" +
                "Command: profile\n" +
                "Parametrs: -\n" +
                "Description: show profile info\n" +
                "-----------------------------\n" +
                "Command: arena\n" +
                "Parametrs: -\n" +
                "Description: allows you to choose a room for pvp\n" +
                "-----------------------------\n" +
                "Command: shop\n" +
                "Parametrs: -\n" +
                "Description: shows shops";

        public const string ARENA_COMMAND = "\n-----------Arena------------\n" +
                "Command: rooms\n" +
                "Parametrs: -\n" +
                "Description: show active room\n" +
                "-----------------------------\n" +
                "Command: create_room\n" +
                "Parametrs: -\n" +
                "Description: create new game room\n" +
                "-----------------------------\n" +
                "Command: remove_room\n" +
                "Parametrs: <room_id>\n" +
                "Description: delete game room\n" +
                "-----------------------------\n" +
                "Command: enter_room\n" +
                "Parametrs: <room_id>\n" +
                "Descriprion: enter in room";

        public static string ShowProfile(Player player)
        {
            Statistics statistic = State.GetInst().GetStatistic(State.GetInst().GetAccount(player.accLogin).id);
            string toReturn = "\n-----------Profile-----------\n" +
            $"Money: {player.money}\n" +
            $"Win amount: {statistic.wins}\n" +
            $"Lose amount: {statistic.losses}\n" +
            $"Items: ";
            if (player.GetItems() == null || player.GetItems().Count == 0)
                toReturn += "Empty";
            else
                foreach (var item in player.GetItems())
                {
                    if (item.iType == ItemType.WEAPON)
                        toReturn += $"\nWeapon: {item.name}";
                    else if (item.iType == ItemType.CLOTHES)
                        toReturn += $"\nClothes: {item.name}";
                    else
                        toReturn += $"\nRune: {item.name}";
                }

            return toReturn;
        }

        public static string Rooms(List<Room> rooms)
        {
            string toReturn = "\n----------GameRooms----------\n";

            if (rooms == null || rooms.Count == 0)
            {
                toReturn += "No one active room";
                return toReturn;
            }

            foreach (var room in rooms)
            {
                if (room.status != Room.STATUS.END)
                {
                    toReturn += $"Id: {room.id}\n" +
                        $"Players: ";
                    if (room.players.Item1.accLogin == null && room.players.Item2.accLogin == null)
                        toReturn += "Empty";
                    else if (room.players.Item1.accLogin != null)
                        toReturn += $"{room.players.Item1.accLogin} ";
                    else if (room.players.Item2.accLogin != null)
                        toReturn += $"{room.players.Item2.accLogin}";
                    toReturn += $"\nStatus: {room.status}\n" +
                        $"-----------------------------\n";
                }
            }
            return toReturn;
        }

        public static string Shop()
        {

            string toReturn = "\n-------------Shop------------\n" +
                "---Weapons---\n";
            foreach (var weapon in Server.Game.Shop.GetWeapons())
            {
                toReturn += $"Id: {weapon.id}\n" +
                            $"Name: {weapon.name}\n" +
                            $"Damage: {weapon.damage}\n" +
                            $"Price: {weapon.price}\n" +
                            $"----------------\n";
            }

            toReturn += "\n---Clothes---\n";
            foreach (var cloth in Server.Game.Shop.GetClothes())
            {
                toReturn += $"Id: {cloth.id}\n" +
                            $"Name: {cloth.name}\n" +
                            $"Health: {cloth.health}\n" +
                            $"Prise: {cloth.price}\n" +
                            $"----------------\n";
            }

            toReturn += "\n----Runes----\n";
            foreach (var rune in Server.Game.Shop.GetRunes())
            {
                toReturn += $"Id: {rune.id}\n" +
                            $"Name: {rune.name}\n" +
                            $"Health: {rune.health}\n" +
                            $"Damage: {rune.damage}\n" +
                            $"Reward: {rune.reward}\n" +
                            $"Prise: {rune.price}\n" +
                            $"----------------\n";
            }

            return toReturn;
        }
    }
}

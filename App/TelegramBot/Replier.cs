﻿using Server.TelegramBot.Comm;
using System.Threading.Tasks;
using System.Threading;
using System;
using Telegram.Bot.Types;
using Telegram.Bot;
using App.Application.Settings;
using App.Core;
using App.Server.JsonReply;

namespace Server.TelegramBot
{
    public class Replier
    {
        private Invoker _invoker;

        public Replier(ref Settings settings, ref TelegramBotClient client)
        {
            _invoker = new Invoker(ref settings, ref client);
        }

        async public Task Update(ITelegramBotClient botClient, Update update, CancellationToken arg3)
        {
            var message = update.Message;

            if (message != null && message.Text != Utlis.SMILENAME)
            {
                _invoker.SetCommand(message);
                _invoker.Response();
            }
        }

        public Task Error(ITelegramBotClient tgClient, Exception ex, CancellationToken cancelTok)
        {
            throw new NotImplementedException($"Undefind exception: {ex.Message}");
        }
    }
}

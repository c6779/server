﻿using App.Core;
using App.Application.Settings;
using System;
using Telegram.Bot;
using App.Server.JsonReply;

namespace Server.TelegramBot
{
    public class TgBot
    {
        public TelegramBotClient client;
        private Replier _replier;

        public TgBot(string token, ref Settings settings)
        {
            client = new TelegramBotClient(token);
            _replier = new Replier(ref settings, ref client);
        }

        public void Start(TelegramBotClient client)
        {
            Logger.GetLog().Info("[TgBot] Bot is running");

            client.StartReceiving(_replier.Update, _replier.Error);
            Console.ReadLine();
        }
    }
}

## Server
---
### ___This is my first project written in c# programming language___
---
### _Purpose_
```
This server is designed to keep running and respond to requests from the client I wrote.
(the link to the repository will be attached below)
```
---
### _Commands_
```
Below is a list of commands that it can respond to:
- registration <loggin, pasword>
   Request: registration dima 1111
   Response: Registration was succesful;
- loggin <loggin, pasword>
   Request: loggin dima 1111
   Response: Aoutorization was succesful;
- active_users (without params) 
   Request: active_users
   Response: Id - 1 Loggin - dima LastPing - 1668279268;
- create_poll <poll_name, umber_voite, user_id>
   Request: create_poll WriteCode 10 1
   Response: Poll was created;
- voite <poll_name, voit, user_id> after a successful vote answer["Voiting was succesful"];
   Request: voite WriteCode 1 1
   Response: Voiting was succesful;
- polls (without params) if there is at least one vote [poll_name, voite_number, info_about_voite];
   Request: polls
   Response: WriteCode 10[true]
- ping <user_id>
   Request: ping 1
   Response: Client is active.
- game (without params)
   Request: game
   Response: 
   ---------GameCommands--------

   Command: profile
   Parametrs: -
   Description: show profile info
   -----------------------------
   Command: arena
   Parametrs: -
   Description: allows you to choose a room for pvp
   -----------------------------
   Command: shop
   Parametrs: - 
   Description: show shop
- profile (without params)
   Request: profile
   Response: 
   -----------Profile-----------
   Money: 0
   Win amount: 0
   Lose amount: 0
- arena (without params)
   Request: arena
   Response: 
   -----------Arena------------
   Command: rooms
   Parametrs: -
   Description: show active room
   -----------------------------
   Command: create_room
   Parametrs: -
   Description: create new game room
   -----------------------------
   Command: remove_room
   Parametrs: <room_id>
   Description: delete game room
   -----------------------------
   Command: enter_room
   Parametrs: <room_id>
   Descriprion: enter in room
- shop (without params)
   Request: shop
   Response: -------------Shop------------
   ---Weapons---
   Id: 3
   Name: tungsten_sword
   Damage: 44
   Price: 500
   ----------------
   Id: 2
   Name: magic_wand
   Damage: 25
   Price: 100
   ----------------
   Id: 1
   Name: metal_sword
   Damage: 17
   Price: 50
   ----------------
- rooms (without params)
   Request: rooms
   Response:
   ----------GameRooms----------
   Id: 1
   Players: Empty
   Status: WAIT_PLSYERS
   -----------------------------
   Id: 8
   Players: Empty
   Status: WAIT_PLSYERS
   -----------------------------
   Id: 16
   Players: Empty
   Status: WAIT_PLSYERS
   -----------------------------
   Id: 17
   Players: Empty
   Status: WAIT_PLSYERS
   -----------------------------
- create_room (without params)
   Request: create_room
   Response: Room has been created
- remove_room <room_id>
   Request: remove_room 1
   Response: Room has been create
- enter_room <room_id>
   Request: enter_room 34
   Response: Start fight: dima(100) vs telegatop0(169)
- buy <item_id>
   Request: buy 3
   Response: Purchase completed successfully
```
Responses are also provided in case of exceptions.
At the moment these are all commands that the server can respond to.
At the end, a link to the full description of the project and the above
commands will be attached.

---
### _Work example_

![work example](https://i.imgur.com/yjDD49P.png)
![work example](https://i.imgur.com/XTLHhpF.png)
---
### _TgBot_

Telegram bot implements all methods that are available on the server.
Telegram users are authorized by default.
It can be found by nickname [@dont_call_me_pls_bot](https://t.me/dont_call_me_pls_bot)
---
### _Game_

The game is available both on console and telegram.
Console users need to be logged in to play.
To make the game more interesting, game items have been 
added that can be bought in the game store.
The game starts after the player joins one of the available rooms.

![game example](https://i.imgur.com/yaaA9jv.png)
---
### _Links_
- [Link to client repository](https://gitlab.com/c6779/client)
- [Link to the full description of the project](https://docs.google.com/document/d/1bsQ0ZEBnM-VsMyh965cZgceav1-NSgfky9RH6HX3aIY/edit?usp=sharing)
- [Help in writing a project](https://gitlab.com/dimmarvel)
